package com.micro.serviceregistry.dto;

public class UserLoginSignupDTO {
	
	private String firstName;
	private String password;
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Override
	public String toString() {
		return "UserLoginSignupDTO [firstName=" + firstName + ", password=" + password + "]";
	}

		
}
